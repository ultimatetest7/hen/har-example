FROM python:3.8

RUN \
    apt-get update \
    && apt-get install -y \
    build-essential
 
RUN \
    python -m pip install --upgrade pip \
    && python -m pip install wheel

ADD . /app

WORKDIR /app

RUN python -m pip install -r requirements.txt

CMD ["python", "./rest_target.py"]
